package fr.epita.android.whatilike;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mariustanawatsamo on 22/11/2016.
 */

public class detailcontroller extends Fragment{
    ListView mListView;

    // newInstance constructor for creating fragment with arguments
    public static detailcontroller newInstance() {
        detailcontroller fragmentFirst = new detailcontroller();
        Bundle args = new Bundle();
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details, container, false);

        List<controllerlike> data = new ArrayList<>();
        data.add(new controllerlike("Reading", 1, "I like reading books"));
        data.add(new controllerlike("Sport", 0, "I like sports" ));
        data.add(new controllerlike("Gaming", 1, "I love play FPS games"));
        data.add(new controllerlike("Fapping", 1, "I like fapping is good for mood"));
        data.add(new controllerlike("Fighting", 0, "I like figting " ));
        mListView = (ListView) view.findViewById(R.id.activity_main_list_object);

        final Adaptercontroller adapt = new Adaptercontroller(data, view.getContext());
        mListView.setAdapter(adapt);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            AlertDialog popupReset;
            String param;
            DialogInterface.OnClickListener alertListener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int whichButton) {
                    if (dialogInterface.equals(popupReset)) {
                        if (whichButton == DialogInterface.BUTTON_POSITIVE) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("https://www.google.fr/search?q=" + param));
                            startActivity(intent);
                        }
                    }
                }
            };

            @Override
            public void onItemClick(
                    AdapterView<?> adapterView,
                    View clickedView,
                    int clickedRowposition,
                    long clickedRowId) {

                param = "";
                AlertDialog.Builder popupBuilder = new AlertDialog.Builder(clickedView.getContext());
                popupBuilder.setTitle("Why?");
                popupBuilder.setMessage(adapt.getItem(clickedRowposition).getComment());
                popupBuilder.setPositiveButton("Search", alertListener);
                popupBuilder.setNegativeButton("Cancel", null);
                popupReset = popupBuilder.create();
                popupReset.setButton(AlertDialog.BUTTON_POSITIVE, "Search", alertListener);
                popupReset.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", alertListener);
                param = adapt.getItem(clickedRowposition).getName();
                popupReset.show();
            }


        });
        return view;
    }

}
