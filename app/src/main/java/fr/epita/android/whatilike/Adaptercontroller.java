package fr.epita.android.whatilike;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mariustanawatsamo on 22/11/2016.
 */

public class Adaptercontroller extends BaseAdapter {
    private List<controllerlike> data;
    private Context context;

    public Adaptercontroller(List<controllerlike> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public controllerlike getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder {
        final TextView nameTextview;
        final android.widget.ImageView ImageView;

        public ViewHolder(View rowView) {
            nameTextview = (TextView) rowView.findViewById(R.id.object_name);
            ImageView = (ImageView) rowView.findViewById(R.id.object_thumb);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        controllerlike currentItem = getItem(position);
        ViewHolder viewholder;
        View rowView;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.list, parent, false);
            viewholder = new ViewHolder(rowView);
            rowView.setTag(viewholder);
        } else {
            rowView = convertView;
            viewholder = (ViewHolder) convertView.getTag();
        }

        viewholder.nameTextview.setText(currentItem.getName());
/*
        int id = getResources().getIdentifier("@drawable/thumb_down.png", null, null);
        viewholder.ImageView.setImageResource(id);
*/
        return rowView;
    }
}
