package fr.epita.android.whatilike;

/**
 * Created by mariustanawatsamo on 22/11/2016.
 */

public class controllerlike {
    private String name;
    private int like;
    private String comment;

    public controllerlike(String name, int like, String comment) {
        this.name = name;
        this.like = like;
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public int getLike()  {
        return like;
    }
}

