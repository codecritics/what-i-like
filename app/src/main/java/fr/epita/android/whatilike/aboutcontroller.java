package fr.epita.android.whatilike;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by mariustanawatsamo on 22/11/2016.
 */

public class aboutcontroller extends Fragment{
    // newInstance constructor for creating fragment with arguments
    public static aboutcontroller newInstance() {
        aboutcontroller fragmentFirst = new aboutcontroller();
        Bundle args = new Bundle();
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about, container, false);

        return view;
    }

}
