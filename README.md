# README de "What I Like"
![alt text](https://gitlab.com/codecritics/what-i-like/raw/master/app/src/main/res/drawable/what-i-like.png)

## But du projet
Valider nos acquis après 15h de cours d'initation de programmation Android

* Créer une application multi-vue
* L'application a pour but d'afficher les centres d'interêt d'un utilisateur Like/Don't Like (voir les apercus ci-dessous)

### Installation
* Grade
* JAVA 1.7 ou plus
* Android Studio
* Gennymotion ou autres...

### Lancement

* `https://gitlab.com/codecritics/what-i-like`
* Importer le dossier "what-i-like" dans android studio.
* Télécharger les composantes manquantes et compiler.

### Aperçu
![alt text](https://gitlab.com/codecritics/what-i-like/raw/master/app/src/main/res/drawable/screen-1.png)
![alt text](https://gitlab.com/codecritics/what-i-like/raw/master/app/src/main/res/drawable/screen-2.png)
![alt text](https://gitlab.com/codecritics/what-i-like/raw/master/app/src/main/res/drawable/screen-3.png)
![alt text](https://gitlab.com/codecritics/what-i-like/raw/master/app/src/main/res/drawable/screen-4.png)

### Utilisation
Afin de voir comment utiliser l'application web se referer au rapport
